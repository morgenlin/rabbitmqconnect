# RabbitmqConnect介绍
#### 1、RabbitmqConnect是基于C++11实现的简单易用的rabbitmq客户端。

#### 2、源码只包含一个头文件与一个示例代码，无需编译安装，真正做到零依赖。

#### 3、RabbitmqConnect针对[RabbitMQ C Client](https://github.com/alanxz/rabbitmq-c)进行封装，提供给用户更友好的接口。
#### 4、RabbitmqConnect追求极简易用，只提供了以下方法：
* `send`发送消息
* `recv`接收消息
* `login`身份认证
* `close`关闭连接
* `connect`连接服务器

# 安装方法
#### 1、下载源码

git clone https://gitee.com/xungen/rabbitmqconnect.git

#### 2、直接在工程中包含[RabbitmqConnect.h](https://gitee.com/xungen/rabbitmqconnect/blob/master/RabbitmqConnect.h)头文件

#### 3、利用RabbitmqConnect发送与接收消息示例代码如下

##### 1.发送消息
```
RabbitmqConnect conn;

if (conn.connect(host, port) && conn.login(user, passwd))
{
	cout << "连接消息队列成功" << endl;

	if (conn.send(exchange, queuename, "message") < 0)
	{
		cout << "发送消息成功" << endl;
	}
	else
	{
		cout << "发送消息失败" << endl;
	}
}
```

##### 2.接收消息
```
while (true)
{
	RabbitmqConnect conn;

	if (conn.connect(host, port) && conn.login(user, passwd))
	{
		cout << "开始监听消息队列[" << exchange << "][" << queuename << "]" << endl;

		while (true)
		{
			conn.recv(exchange, queuename, [](const char* data, int len){
				string msg(data, data + len);

				cout << "收到消息[" << msg << "]" << endl;
			});
			
			//连接异常需要重连
			if (conn.getErrorCode() == AMQP_STATUS_SOCKET_ERROR) break;
		}

		cout << "监听消息队列失败[" << conn.getErrorString() << "]" << endl;
	}
	else
	{
		cout << "连接消息队列[" << host << ":" << port << "][" << user << "]失败" << endl;
	}

	sleep(5);
}
```
<br><br>
##### <a href='https://www.rabbitmq.com' target='_blank'>RabbitMQ官网</a>
##### <a href='https://www.rabbitmq.com/devtools.html' target='_blank'>RabbitMQ客户端</a>
##### <a href='https://github.com/alanxz/rabbitmq-c' target='_blank'>RabbitMQ C Client下载</a>
